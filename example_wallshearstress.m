%% Prepare MATLAB
% --- Author information
% Wouter Potters
% Academic Medical Center, Amsterdam, The Netherlands
% w.v.potters@amc.nl
% Date: 10-September-2014

clear all; close all; clc;
path_to_this_file = ''; % change me to something like 'C:/dir/to/example/'
if isempty(path_to_this_file) || exist(path_to_this_file,'dir') ~= 7, errordlg('Please change code line 9 of example_wallshearstress.m and run example_wallshearstress.m as a script.'); end % check path
addpath(genpath(path_to_this_file)) % add the 'include' directory to the matlab work path
cd(path_to_this_file)

% check matlab version and toolboxes
versions = ver;
if ~any(strcmp({versions.Name},'Curve Fitting Toolbox'))
    errordlg('You might need the curve fitting toolbox for this Wall Shear Stress code to work.')
end
MLversion = num2str(cell2mat(regexp(versions((strcmp({versions.Name},'MATLAB'))).Date,'20..','match')));
if MLversion < 2014
    errordlg('You might need a newer Matlab version for some portions of the code to work...')
end

%% Load DICOM and STL and save data to MAT file
% DICOM LOADING AND STL LOADING NOT INCLUDED IN THIS EXAMPLE
% reason: it might not work with all dicom/STL formats out there. To avoid
%         cluttered code and unclarity it is not included in this wall 
%         shear stress code repository.
%
%         especially different software with different coordinate systems
%         depending on the orientation of the dicom volume can be
%         ambiguous.
%
% What I did was the following:
% A1. Load dicom files
% A2. Correct velocities for linear phase offset errors (linear plane fits using
%     surrounding muscle tissue)
% B1. import dicom in ITK-snap
% B2. segment using ITK-SNAP manually (on both phase difference and
%      magnitude images)
% B3. Export STL file from ITK-SNAP
% B4. Refine mesh/ smooth mesh using either Matlab code or the Vessel
%      Modeling Toolkit (VMTK.org).
% C1. Load B4 and A2 for WSS calculation. - using dicomread for dicom files
%      and [faces,vertices] = stlread('stlfile.stl')

%% Load the dataset and the surface
% Load single timestep from a dicom dataset from *.mat file
clear all;
load('example_carotid_single_timestep.mat'); % load dataset

% Convert units to SI units (meter and second)
x = x * 10^-3; % mm to meter for x,y,z
y = y * 10^-3;
z = z * 10^-3;

vertices = vertices * 10^-3; % mm to meter for location of vessel wall

velx = images.phase_difference_RL * 10 ^ -2; % cm/s to meter/s for velocities
vely = images.phase_difference_AP * 10 ^ -2;
velz = images.phase_difference_FH * 10 ^ -2;

%% Calculate wall shear stress
% This may take quite a while, depending on your computer and the size of
% the surface mesh. Here it takes approx. 15 minutes for 46779 surface mesh 
% points using a 2.3GHz Intel Core I7 processor with parallel processing
% options turned on (requires parallel processing toolbox).
inward_normal_length = 10^-2;
inward_normal_length_nr_points = inward_normal_length/3; % 3 points;
% WSS_object = calculate_wss_3d(x,y,z,velx,vely,velz,'blood',faces,vertices,inward_normal_length,inward_normal_length_nr_points);
% save('example_wss_results.mat','WSS_object')

%% Create figure with magnitude image and segmentation
load example_wss_results;
fh = figure(1); clf; set(fh,'Color','w','Position',[1 1 800 800]);% create figure
set(gcf,'Renderer','OpenGL'); % this gives the best visualisation results.
hold on; % plot multiple plots in 1 figure

patch('faces',faces,'vertices',vertices,'facecolor','r','edgecolor','none'); % plot the segmentation (from stl file as a surface)
xlabel('x (meter)'); ylabel('y (meter)'); zlabel('z (meter)'); % turn on axis labels

% Plot one crosssectional slice
xsel = 80:130; ysel = 100:170; zsel = 22;
s(1) = surf(x(xsel,ysel,zsel),y(xsel,ysel,zsel),z(xsel,ysel,zsel),images.magnitude(xsel,ysel,zsel),'edgecolor','none'); 
% Plot second crosssectional slice
xsel = 80:130; ysel = 143; zsel = 1:size(x,3); 
s(2) = surf(squeeze(x(xsel,ysel,zsel)),squeeze(y(xsel,ysel,zsel)),squeeze(z(xsel,ysel,zsel)),squeeze(images.magnitude(xsel,ysel,zsel)),'edgecolor','none'); 
% note that the surf/ mesh command actually shifts the voxels by 0.5 pixels

lighting('gouraud') % change algorithm used for lighting
axis equal tight vis3d; % make sure x,y and z and equally scaled

view(64,19); zoom(.9); rotate3d on % set an angled view

ti = title({'Two slices of the PC MRI magnitude image (shifted by 0.5px)','& the segmented vessel surface in red','Coordinate system: scanner'});
set(ti,'Position',get(ti,'Position')+[0 0 -5])

camlight headlight; % turn on a light to show that the segmentation is 3D

%% Create figure with segmentation surface colored according to the WSS vectors
load example_wss_results;
wss_mag = sqrt(sum(WSS_object.wallshearstress.^2,2)); 

fh = figure(2); clf; set(fh,'Color','w','Position',[1 1 800 800]);% create figure
set(gcf,'Renderer','OpenGL'); % this gives the best visualisation results.
hold on; % plot multiple plots in 1 figure

pa = patch('faces',faces,'vertices',vertices,'facecolor','interp','FaceVertexCdata',wss_mag,'EdgeColor','none');
xlabel('x (meter)'); ylabel('y (meter)'); zlabel('z (meter)'); % turn on axis labels
caxis([0 nanmax(wss_mag)])

lighting('gouraud') % change algorithm used for lighting
axis equal tight vis3d; % make sure x,y and z and equally scaled

view(64,19); zoom(.5); rotate3d on % set an angled view

cb = colorbar;set(cb,'fontsize',20);

ti = title({'Segmented vessel surface in color of WSS magnitude','Coordinate system: scanner'});
[a1,a2] = view;
speed = 2;

for angl = 1:speed:180
    view(a1+angl,a2)
    drawnow
end

%% Visualise the wall shear stress as arrows on the vessel wall
load example_wss_results;
wss_mag = sqrt(sum(WSS_object.wallshearstress.^2,2)); 

fh = figure(3); clf; set(fh,'Color','w','Position',[1 1 800 800]);% create figure
set(gcf,'Renderer','OpenGL'); % this gives the best visualisation results.
hold on; % plot multiple plots in 1 figure

nr_of_vectors_to_plot = 3000;
selection_to_plot = randi([1 length(vertices)],[1 nr_of_vectors_to_plot]); % randomly select 1000 vectors to plot for visualization.
scaling_arrows = [0.001 .003];
[faces_arrows,vertices_arrows,colors_arrows] = quiver3Dpatch( vertices(selection_to_plot,1),vertices(selection_to_plot,2),vertices(selection_to_plot,3), WSS_object.wallshearstress(selection_to_plot,1),WSS_object.wallshearstress(selection_to_plot,2),WSS_object.wallshearstress(selection_to_plot,3),[],scaling_arrows);
patch('faces',faces_arrows,'vertices',vertices_arrows,'Cdata',colors_arrows,'FaceColor','flat','edgealpha',1,'edgecolor',[.2 .2 .2])

xlabel('x (meter)'); ylabel('y (meter)'); zlabel('z (meter)'); % turn on axis labels
caxis([0 nanmax(wss_mag)])

lighting('gouraud') % change algorithm used for lighting
axis equal tight vis3d; % make sure x,y and z and equally scaled

view(64,19); zoom(.5); rotate3d on % set an angled view

colormap(jet(100))
cb = colorbar;set(cb,'fontsize',20);

ti = title({'Segmented vessel surface in color of WSS magnitude','Coordinate system: scanner'});
[a1,a2] = view;
speed = 10;
for angl = 1:speed:180
    view(a1+angl,a2)
    drawnow
end

