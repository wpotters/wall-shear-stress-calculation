function [shearstress_object,surface_faces,surface_vertices] = calculate_wss_3d( datax, datay, dataz, velocity_x, velocity_y, velocity_z, viscosity_medium, surface_faces, surface_vertices, length_inward_normal_in_m, distance_per_point_in_m )
% [shearstress_object,surface_faces,surface_vertices] = calculate_wss_3d( ...
%     datax, datay, dataz,...               % coordinates x,y,z in meter
%     velocity_x,velocity_y,velocity_z, ... % velocity components x,y,z in meter/s
%     viscosity_medium,...                  % viscosity_medium 'water' or 'blood' 
%     surface_faces,surface_vertices,...    % input surface elements/faces (triangles) and coordinates/vertices in mm
%     length_inward_normal_in_m,...         % lenght of the inward normal that is used in meter (usually +/- 50% of the diameter)
%     distance_per_point_in_m )             % distance per inward point; usually chosen as: ( (50% diameter) /3 )
% ---
% NOTE 1: UNITS OF INPUT DATA ARE VERY IMPORTANT 
% NOTE 2: Please make sure your segmentation is as accurate as possible
% ---
% Method is described in this article: 
% Potters, W. V., van Ooij, P., Marquering, H., vanBavel, E. and Nederveen, A. J. (2014), Volumetric arterial wall shear stress calculation based on cine phase contrast MRI. J. Magn. Reson. Imaging. doi: 10.1002/jmri.24560
% link: http://onlinelibrary.wiley.com/doi/10.1002/jmri.24560/full
% ---
% See also this review article:
% Potters WV, Marquering HA, VanBavel E, Nederveen AJ. Measuring Wall Shear Stress Using Velocity-Encoded MRI. Curr Cardiovasc Imaging Rep 2014;7:9257. doi: 10.1007/s12410-014-9257-1.
% http://link.springer.com/article/10.1007/s12410-014-9257-1

% ---
% This method was used in these articles:
% 1. van Ooij P, Potters WV, Collins J, Carr M, Carr J, Malaisrie SC, Fedak PWM, McCarthy PM, Markl M, Barker AJ. Characterization of Abnormal Wall Shear Stress Using 4D Flow MRI in Human Bicuspid Aortopathy. Ann Biomed Eng. 2014:1?13. doi: 10.1007/s10439-014-1092-7.
% 2. Cibis M, Potters WV, Gijsen FJH, Marquering H, VanBavel E, van der Steen AFW, Nederveen AJ, Wentzel JJ. Wall shear stress calculations based on 3D cine phase contrast MRI and computational fluid dynamics: a comparison study in healthy carotid arteries. NMR Biomed. 2014:n/a?n/a. doi: 10.1002/nbm.3126.
% 3. van Ooij P, Potters WV, Nederveen AJ, Allen BD, Collins J, Carr J, Malaisrie SC, Markl M, Barker AJ. A methodology to detect abnormal relative wall shear stress on the full surface of the thoracic aorta using four?dimensional flow MRI. Magn. Reson. Med. 2014:n/a?n/a. doi: 10.1002/mrm.25224.
% 4. van Ooij P, Potters WV, Gu?don A, Schneiders JJ, Marquering HA, Majoie CB, vanBavel E, Nederveen AJ. Wall shear stress estimated with phase contrast MRI in an in vitro and in vivo intracranial aneurysm. J. Magn. Reson. Imaging [Internet] 2013:1?16. doi: 10.1002/jmri.24051.

% --- Author information
% Wouter Potters
% Academic Medical Center, Amsterdam, The Netherlands
% w.v.potters@amc.nl (questions, bugs, etc.)
% Date: 10-September-2014

%% STEP 0: CHECK INPUTS
% check if input has same size all around
if ~isequaln(size(datax),size(datay),size(dataz),size(velocity_x),size(velocity_y),size(velocity_z))
    disp([size(datax),size(datay),size(dataz),size(velocity_x),size(velocity_y),size(velocity_z)])
    error('Please make sure that position and velocity input all have the same size.')
end

% check if data is in matrix form
if (length(size(datax)) > 2) || (~any(size(datax) == 1)) % verify that data is in matrix form
    cornerpoints = [datax(  1,  1,  1) datay(  1,  1,  1) dataz(  1,  1,  1);...
        datax(  1,  1,end) datay(  1,  1,end) dataz(  1,  1,end);...
        datax(  1,end,  1) datay(  1,end,  1) dataz(  1,end,  1);...
        datax(end,  1,  1) datay(end,  1,  1) dataz(end,  1,  1);...
        datax(end,end,  1) datay(end,end,  1) dataz(end,end,  1);...
        datax(end,  1,end) datay(end,  1,end) dataz(end,  1,end);...
        datax(  1,end,end) datay(  1,end,end) dataz(  1,end,end);...
        datax(end,end,end) datay(end,end,end) dataz(end,end,end)];
    cornerpoints_faces = [1 2 7 3;...
        3 7 8 5;...
        5 8 6 4;...
        4 6 2 1;...
        8 6 2 7;...
        5 4 1 3];
    
    % convert matrix -> array
    datax = datax(:); datay = datay(:); dataz = dataz(:);
    velocity_x = velocity_x(:); velocity_y = velocity_y(:); velocity_z = velocity_z(:);
    
else % only use the most outer points
    cornerpoints = [min(datax(:)) min(datay(:)) min(dataz(:));... 1
        max(datax(:)) max(datay(:)) max(dataz(:));... 2
        max(datax(:)) min(datay(:)) min(dataz(:));... 3
        min(datax(:)) max(datay(:)) min(dataz(:));... 4
        min(datax(:)) min(datay(:)) max(dataz(:));... 5
        max(datax(:)) max(datay(:)) min(dataz(:));... 6
        min(datax(:)) max(datay(:)) max(dataz(:));... 7
        max(datax(:)) min(datay(:)) max(dataz(:))]; % 8
    cornerpoints_faces = [1 3 6 4;...
        5 8 2 7;...
        1 5 7 4;...
        4 6 2 7;...
        3 8 2 6;...
        1 5 8 3];
end

% check and correct surface faces / vertices
[surface_faces,surface_vertices] = cleanupFV(surface_faces,surface_vertices);

% calculate normal vectors - uses external function
n = patchnormals(struct('faces',surface_faces,'vertices',surface_vertices));

% check validity of MESH vs. velocity data points
condition1 = min(surface_vertices) >= [min(datax) min(datay) min(dataz)] - eps(min([min(datax) min(datay) min(dataz)]));
condition2 = max(surface_vertices) <= [max(datax) max(datay) max(dataz)] + eps(max([max(datax) max(datay) max(dataz)]));

if ~all(condition1) && ~all(condition2)
    disp(['lower - ' num2str(find(~condition1)) ' are wrong (x,y,z) - lower'])
    disp(['upper - ' num2str(find(~condition2)) ' are wrong (x,y,z) - upper'])

    if sqrt(sum((min(surface_vertices) - [min(datax) min(datay) min(dataz)]).^2) + sum((max(surface_vertices) + [max(datax) max(datay) max(dataz)]).^2)) > 0.3
        error('The lower and upper boundaries of the surface exceed that of the velocity datapoints.')
    end
elseif ~all(condition2)
    disp(['upper bounds - ' num2str(find(~condition2)) ' are wrong (x,y,z)'])
    if sqrt(sum((min(surface_vertices) - [min(datax) min(datay) min(dataz)]).^2) + sum((max(surface_vertices) + [max(datax) max(datay) max(dataz)]).^2)) > 0.3
        error('The upper boundaries of the surface exceed that of the velocity datapoints.')
    end
    
elseif ~all(condition1)
    disp(['lower bounds - ' num2str(find(~condition1)) ' are wrong (x,y,z)'])
    if sqrt(sum((min(surface_vertices) - [min(datax) min(datay) min(dataz)]).^2) + sum((max(surface_vertices) + [max(datax) max(datay) max(dataz)]).^2)) > 0.3
        error('The lower boundaries of the surface exceed that of the velocity datapoints.')
    end
end

%% STEP 1: SELECT POINTS INSIDE MESH
% This gives a selection of the relevant points inside the current surface.
% Saves a lot of time during scatteredInterpolation in STEP 2.
points_inside_mesh = isPointInsideMesh(surface_faces,surface_vertices,n,[datax,datay,dataz],'inside');
% Watch out with the normal directions / orientation of surface triangles;
% they may lead to inverted meshes and thus reversing of 'inside mesh' and 
% 'outside mesh' coordinates.

ttt = tic;
disp('================================================================================')

length_inward_normal_in_m = length_inward_normal_in_m .* ones(length(surface_vertices),1);
number_of_points_on_normal = round(length_inward_normal_in_m./distance_per_point_in_m);
length_inward_normal_in_m  = distance_per_point_in_m.*number_of_points_on_normal;

disp(['Inward normal length: ' num2str(mean(length_inward_normal_in_m*1000)) '+/-' num2str(std(length_inward_normal_in_m*1000) * (std(length_inward_normal_in_m) > 10^-10)) ' millimeter (' num2str(mean(number_of_points_on_normal)) '+/-' num2str((std(number_of_points_on_normal)) * (std(number_of_points_on_normal) > 10^-10)) ' points)'])
disp([' - Calculation complete step 1; total time passed: ' num2str(toc(ttt)/60) ' minutes'])

%% STEP 2: DATA INTERPOLATION
disp('================================================================================')
disp('Step 2: interpolating velocity data. - takes a while');

interpolation_method = 'natural'; % can be faster with 'linear', but this is less accurate
extrapolation_method = 'nearest'; % usually outside the mesh = zero 
                                  % very tricky if you have a misaligned
                                  % mesh -> make sure your input is correct
datax = [datax(points_inside_mesh(:)); surface_vertices(:,1)];
datay = [datay(points_inside_mesh(:)); surface_vertices(:,2)];
dataz = [dataz(points_inside_mesh(:)); surface_vertices(:,3)];

%%% VELOCITY DATA - add zeros at the positions of the wall
velocity_x = [velocity_x(points_inside_mesh(:)); zeros(size(surface_vertices,1),1)];
velocity_y = [velocity_y(points_inside_mesh(:)); zeros(size(surface_vertices,1),1)];
velocity_z = [velocity_z(points_inside_mesh(:)); zeros(size(surface_vertices,1),1)];

if any(isnan(velocity_x(:))) ||  any(isnan(velocity_y(:))) || any(isnan(velocity_z(:)))
    error('calculate_wss_3d:Unexpected NaN found in the data.'); % ABORT
end

% create triangulation of the mesh and create interpolation function based
% on that using scatteredInterpolant - requires relatively recent matlab
% version.
% Note that scatteredInterpolant is quite slow (slower then
% griddedInterpolant), but as we added zeros at the vessel wall we still
% need the scatteredInterpolant functionality with natural neighbor
% interpolation for reliable and C2 continuous interpolation results.
% as described in the JMRI 2014 paper
Fx = scatteredInterpolant(datax(:), datay(:), dataz(:), velocity_x(:),interpolation_method, extrapolation_method);
Fy = scatteredInterpolant(datax(:), datay(:), dataz(:), velocity_y(:),interpolation_method, extrapolation_method);
Fz = scatteredInterpolant(datax(:), datay(:), dataz(:), velocity_z(:),interpolation_method, extrapolation_method);

% Calculate the coordinates of the points at which the velocity will be
% interpolated.
position_along_normal = cell(size(number_of_points_on_normal,1),2);
for iPos = 1:size(number_of_points_on_normal,1)
    position_along_normal{iPos,1} = (length_inward_normal_in_m(iPos) ./ number_of_points_on_normal(iPos)) * (1:number_of_points_on_normal(iPos));
    position_along_normal{iPos,2} = number_of_points_on_normal(iPos);
end

disp(['interpolation functions created; total time passed: ' num2str(toc(ttt)/60) ' minutes'])
disp(['calculating values for measurement points...'])

direction_of_normal     = cell(size(n,1),1);
originating_points      = cell(size(n,1),1);
measurement_coordinates = cell(size(n,1),1);

% Loop over all interpolation coordinates to calculate velocity at thos
% points.
for i_v = 1:size(n,1)
    direction_of_normal{i_v,1,1} = n(i_v,1) * position_along_normal{i_v,1};
    direction_of_normal{i_v,1,2} = n(i_v,2) * position_along_normal{i_v,1};
    direction_of_normal{i_v,1,3} = n(i_v,3) * position_along_normal{i_v,1};
    
    originating_points{i_v,1,1} = surface_vertices(i_v,1) * ones(1,position_along_normal{i_v,2});
    originating_points{i_v,1,2} = surface_vertices(i_v,2) * ones(1,position_along_normal{i_v,2});
    originating_points{i_v,1,3} = surface_vertices(i_v,3) * ones(1,position_along_normal{i_v,2});
    
    measurement_coordinates{i_v,1,1} = originating_points{i_v,1,1} + direction_of_normal{i_v,1,1}; %[ X nrofpointsalongnormal 3 ]
    measurement_coordinates{i_v,1,2} = originating_points{i_v,1,2} + direction_of_normal{i_v,1,2}; %[ X nrofpointsalongnormal 3 ]
    measurement_coordinates{i_v,1,3} = originating_points{i_v,1,3} + direction_of_normal{i_v,1,3}; %[ X nrofpointsalongnormal 3 ]
    if rem(i_v,10000) == 0
        disp([num2str(round((i_v/size(n,1))*100)) '% done... total time passed: ' num2str(toc(ttt)/60) ' minutes'])
    end
end

clear originating_points direction_of_normal %not needed; clear memory

%%% Interpolation of velocity values at normal vectors using natural triangular interpolation
velocity_values_interpolated_x = cell(size(surface_vertices,1),1);
velocity_values_interpolated_y = velocity_values_interpolated_x;
velocity_values_interpolated_z = velocity_values_interpolated_x;

disp(['Now calculating velocities using the created interpolation functions for ' num2str(size(velocity_values_interpolated_x,1)) ' points...'])
% put this stuff in an array to optimize the parfor loop
me_co_array_x = cat(1,measurement_coordinates{:,1,1});
me_co_array_y = cat(1,measurement_coordinates{:,1,2});
me_co_array_z = cat(1,measurement_coordinates{:,1,3});

% try interpolation using parfor loop - faster then the for loop - parallel
%  processing toolbox required + running 'matlabpool open' before this line
%parfor k = 1:size(measurement_coordinates,1) 
for k = 1:size(measurement_coordinates,1)
    mx = me_co_array_x(k,:);
    my = me_co_array_y(k,:);
    mz = me_co_array_z(k,:);
    velocity_values_interpolated_x{k,1} = Fx(mx,my,mz); 
    velocity_values_interpolated_y{k,1} = Fy(mx,my,mz); 
    velocity_values_interpolated_z{k,1} = Fz(mx,my,mz); 
end
clear me_co_array_x me_co_array_y me_co_array_z Fx Fy Fz

disp([' - Interpolation complete step 2; total time passed: ' num2str(toc(ttt)/60) ' minutes'])

%% STEP 3: Calculating rotation matrices for each point
disp('================================================================================')
disp('Step 3: calculating rotation matrices for each point on the wall.')
%reshape to [X nrofpointsalongnormal 3]
velocity_values_interpolated = cat(3,velocity_values_interpolated_x,velocity_values_interpolated_y,velocity_values_interpolated_z);

%%%% define axes
xaxis  = [ 1 0 0];
yaxis  = [ 0 1 0];
zaxis  = [ 0 0 1];

%nrofpoints = size(c,1);
nrofpoints = size(surface_vertices,1);

%%% CALCULATE THE NEW X AND Y AXIS IN EACH POINT - ALIGN X AXIS WITH NORMAL
%%% VECTOR n IN EACH COORDINATE (RODRIGUES' ROTATION FORMULA)
% as described in the JMRI 2014 paper
% Uses some vector calculus and matlab tricks to speed up the process
k        = cross(repmat(zaxis',[1 nrofpoints]),n'); %[3 X]
costheta =   dot(repmat(zaxis',[1 nrofpoints]),n'); %[X 1]
C  = reshape(repmat(costheta(1:nrofpoints),[9 1]),[3 3 nrofpoints]) .* repmat(eye(3,3),[1 1 nrofpoints]);
R_ = [             zeros(1,1,nrofpoints)     -reshape(k(3,:),[1 1 nrofpoints])   reshape(k(2,:),[1 1 nrofpoints]) ; ...
    reshape(k(3,:),[1 1 nrofpoints])               zeros(1,1,nrofpoints)   -reshape(k(1,:),[1 1 nrofpoints]) ; ...
    -reshape(k(2,:),[1 1 nrofpoints])     reshape(k(1,:),[1 1 nrofpoints])              zeros(1,1,nrofpoints) ];
%selector matrices j and j2 - use to select k and k'
j  = repmat(1:numel(k),[3 1]); j = j(:); j2 = repmat([1:3:nrofpoints*3; 2:3:nrofpoints*3; 3:3:nrofpoints*3], [3 1]); j2 = j2(:); %selectors to manually perform a row-wise vector product in the next line
K = reshape(k(j2) .* k(j),[3 3 nrofpoints]); %K = k*k'
L_ = (1 - costheta) ...%(1-costheta)
    ./ sum(k.^2,1); %./sum(k.^2)
L  = reshape(repmat(L_(1:nrofpoints),[9 1]),[3 3 nrofpoints]); %[3 3 X]

% Rodrigues' Rotation Formula
R = C +  R_ + K .* L; %calculate rotation matrix to rotate xaxis to match normalvector
clear C R_ K L j j2 costheta k

% NOW WE HAVE  ROTATION MATRIX R; WE USE IT TO CALCULATE THE NEW X,Y AND
% Z AXIS IN EACH POINT AT THE WALL
new_xaxis = zeros(nrofpoints,3);
new_yaxis = zeros(nrofpoints,3);
new_zaxis = zeros(nrofpoints,3);
for i = 1:nrofpoints
    %build new velocity vector in each point, based on new axis, remove new z_axis component
    new_xaxis(i,:) = (R(:,:,i) * xaxis')';
    new_yaxis(i,:) = (R(:,:,i) * yaxis')';
    new_zaxis(i,:) = (R(:,:,i) * zaxis')';
end
disp([' - calculating rotation matrices finished; total time passed: ' num2str(toc(ttt)/60) ' minutes'])

%% STEP 4: Calculating Shear Rates
disp('================================================================================')
disp('Step 4: Calculating Shear Rates')
% pre allocate some variables
new_velocity_x = cell(nrofpoints,1,1);
new_velocity_y = cell(nrofpoints,1,1);

new_velocity_vector = cell(nrofpoints,1,1);

%preallocate memory to speedup forloop
shearrate = zeros(nrofpoints,3);
length_shearrate = zeros(1,nrofpoints);
x = cell(nrofpoints,1);

deri_xy = nan(3,nrofpoints);
splinefit = cell(1,nrofpoints);
total_vel = cell(1,nrofpoints);

disp(['Making fits for ' num2str(nrofpoints) ' points'])
warning off % turn off some warnings regarding wrong spline fits; 
            % this always happens for some splines near the edges

% Actual fitting of the smoothing splines
for i = 1:nrofpoints
    
    x{i,1} = ( 0 : 1/number_of_points_on_normal(i) : 1 ) * length_inward_normal_in_m(i);
    j = 1;
    
    % set all to zero
    new_velocity_vector{i,1}(1:size([x{i,1}],2),1:3) = 0;
    new_velocity_x{i,1}(1:size([x{i,1}],2),1:3) = 0;
    new_velocity_y{i,1}(1:size([x{i,1}],2),1:3) = 0;
    %     %new_velocity_z(i,j,:)     = [ 0 0 0];
    for j = 2:size([x{i,1}],2)
        cur_vel_int = squeeze(cat(3,velocity_values_interpolated{i,1,:}));
        new_velocity_x{i,1}(j,:) = dot(reshape(cur_vel_int(j-1,:),[3 1]),new_xaxis(i,:)') * new_xaxis(i,:);
        new_velocity_y{i,1}(j,:) = dot(reshape(cur_vel_int(j-1,:),[3 1]),new_yaxis(i,:)') * new_yaxis(i,:);
        
        %ignore z' component along inward normal as described in the JMRI 2014 paper
        %new_velocity_z(i,j,:) = dot(reshape(velocity_values_interpolated(i,j-1,:),[3 1]),new_zaxis(i,:)') * new_zaxis(i,:);
        
        new_velocity_vector{i,1}(j,:) = [new_velocity_x{i,1}(j,:)] + [new_velocity_y{i,1}(j,:)];   % New vector in old axes without x from new x axis
    end
    
    total_vel{i} = squeeze([new_velocity_x{i,:,:}] + [new_velocity_y{i,:,:}])';
    try
        splinefit{i} = spaps(x{i,1},... %position along normal
            total_vel{i},... %velocities at locations on normal
            max(sqrt(sum(total_vel{i}.^2,3))/100),... %tolerance of data points %tol 1000 to 100
            [.1 ones(1,size(total_vel{i},2)-1)],... weights first one .1
            3); %quintic
        deri_xy(:,i) = fnval(fnder(splinefit{i}),0 );
    catch
        % nothing
    end
    
    shearrate(i,:) = [ deri_xy(1,i) deri_xy(2,i) deri_xy(3,i)]; % in the original coordinate system, in 1/s
    length_shearrate(i) = sqrt(sum(shearrate(i,:).^2));
end

warning on % turn the warnings back on for other matlab code :)
disp([' - calculating shearrates complete; total time passed: ' num2str(toc(ttt)/60) ' minutes'])

%% final step: SHEAR STRESS CALCULATION
disp('================================================================================')
disp('Step 5: calculation of shear stress complete');
% Save the fitting objects into a shear stress object, so it is easy to
% change inspect the used viscosity values retrospectively.
shearstress_object = shearstress(x,splinefit,shearrate,deri_xy,total_vel,viscosity_medium,1);

disp([' - calculating shearstress complete; total time passed: ' num2str(toc(ttt)/60) ' minutes'])
disp('================================================================================')


% function D = getMaximumDiameter(F,V,N)
% function 'getMaximumDiameter' to determine maximum local diameter based on the input surface 
% - removed - not used in this version of calculate_wss_3d.m